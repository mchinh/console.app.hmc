﻿using System;

namespace ConsoleApp_HMC
{
     class Director: IDetail
    {
        public int id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int CountDay { get; set; }

        const double HsSalary = 12.9;
        public string toString()
        {
            string strings = "*Name:  " + FirstName + " " + LastName + " \t *Role: Director " + "  \t *Salary " + "   " +
                             CountDay * HsSalary * 100000 + " " + "VND" +"\n";
            return strings;
        }
        public Boolean CheckNull(Director ob)
        {

            if (ob.FirstName == "" || ob.LastName == "" ||ob.CountDay<=0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}