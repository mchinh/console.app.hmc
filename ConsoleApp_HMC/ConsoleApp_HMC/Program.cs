﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;


namespace ConsoleApp_HMC
{
    class Program
    {
        static StringBuilder stringBuilder = new StringBuilder();

        static void Main(string[] args)
        {
            CraeteStaffs();
            CraeteManagers();
            CraeteDirectory();

            Thread newThread = new Thread(WriteFile);
            newThread.Start();
        }

        public static void CraeteStaffs()
        {
            List<Staffs> ListStaffs = new List<Staffs>();
            Console.WriteLine("Please enter 02 Staffs:");

            for (int i = 1; i <= 2;)
            {
                var Staffs = new Staffs();
                Console.WriteLine("Staffs  " + i);
                Console.Write("FirstName:");
                Staffs.FirstName = Console.ReadLine();
                CheckName(Staffs.FirstName);
                Console.Write("LastName:");
                Staffs.LastName = Console.ReadLine();

                Console.Write("CountDay:");
                try
                {
                    int CountDay = Convert.ToInt32(Console.ReadLine());
                    Staffs.CountDay = CountDay;
                }
                catch (Exception e)
                {
                    Console.WriteLine(" CountDay must be number, not null");
                }

                if (Staffs.CheckNull(Staffs))
                {
                    Console.WriteLine(" First name,LastName not null ,CountDay must be >0,Please enter again:");
                }
                else
                {
                   
                    ListStaffs.Add(Staffs);
                    i++;
                }
            }

            foreach (var item in ListStaffs)
            {
                Console.WriteLine(item.toString());
                stringBuilder.Append(item.toString());
            }
        }


        public static void CraeteManagers()
        {
            List<Managers> ListManagers = new List<Managers>();
            Console.WriteLine("Please enter 02 Managers:");
            for (int i = 1; i <= 2;)
            {
                var Managers = new Managers();
                Console.WriteLine("Managers  " + i);
                Console.Write("FirstName:");
                Managers.FirstName = Console.ReadLine();
                CheckName(Managers.FirstName);
                Console.Write("LastName:");
                Managers.LastName = Console.ReadLine();

                Console.Write("CountDay:");
                try
                {
                    int CountDay = Convert.ToInt32(Console.ReadLine());
                    Managers.CountDay = CountDay;
                }
                catch (Exception e)
                {
                    Console.WriteLine(" CountDay must be number, not null");
                }

                if (Managers.CheckNull(Managers))
                {
                    Console.WriteLine(" First name,LastName not null ,CountDay must be >0,Please enter again:");
                }
                else
                {
                    ListManagers.Add(Managers);
                    i++;
                }
            }

            foreach (var item in ListManagers)
            {
                Console.WriteLine(item.toString());
                stringBuilder.Append(item.toString());
            }
        }

        public static void CraeteDirectory()
        {
            List<Director> ListDirector = new List<Director>();
            Console.WriteLine("Please enter 02 Director:");
            for (int i = 1; i <= 2;)
            {
                var Director = new Director();
                Console.WriteLine("Director  " + i);
                Console.Write("FirstName:");
                Director.FirstName = Console.ReadLine();
                CheckName(Director.FirstName);
                Console.Write("LastName:");
                Director.LastName = Console.ReadLine();

                Console.Write("CountDay:");
                try
                {
                    int CountDay = Convert.ToInt32(Console.ReadLine());
                    Director.CountDay = CountDay;
                }
                catch (Exception e)
                {
                    Console.WriteLine(" CountDay must be number, not null");
                }

                if (Director.CheckNull(Director))
                {
                    Console.WriteLine(" First name,LastName not null ,CountDay must be >0,Please enter again:");
                }
                else
                {
                    ListDirector.Add(Director);
                    i++;
                }
            }

            foreach (var item in ListDirector)
            {
                Console.WriteLine(item.toString());
                stringBuilder.Append(item.toString());
            }
        }

        public static void WriteFile()
        {
            StreamWriter sWriter = new StreamWriter("name.txt"); //fs là 1 FileStream 
            sWriter.WriteLine(stringBuilder.ToString());
            // Ghi và đóng file
            sWriter.Flush();
            Thread.Sleep(100);
        }

        public static void CheckName(string name)
        {
            if (name=="Anh")
            {
                Console.WriteLine("Existing.....");
                Environment.Exit(0);
            }
        }
    }
}