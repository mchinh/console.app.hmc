﻿using System;

namespace ConsoleApp_HMC
{
    public class Staffs :IDetail
    {
        const double HsSalary = 2.2;
        public int id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int CountDay { get; set; }



        public string toString()
        {
            string strings = "*Name:  " + FirstName + " " + LastName + " \t *Role: Staffs " + "  \t *Salary " + "   " +
                             CountDay * HsSalary * 100000 + " " + "VND" +"\n";
            return strings;
        }

        public Boolean CheckNull(Staffs staffs)
        {

            if (staffs.FirstName == "" || staffs.LastName == "" ||staffs.CountDay<=0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}