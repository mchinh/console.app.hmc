﻿using System;

namespace ConsoleApp_HMC
{
    public class Managers:IDetail
    {
        public int id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int CountDay { get; set; }

        const double HsSalary = 4.6;
        public string toString()
        {
            string strings = "*Name:  " + FirstName + " " + LastName + " \t *Role: Managers " + "  \t *Salary " + "   " +
                             CountDay * HsSalary * 100000 + " " + "VND"+"\n";
            return strings;
        }
        public Boolean CheckNull(Managers Managers)
        {

            if (Managers.FirstName == "" || Managers.LastName == "" ||Managers.CountDay<=0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}