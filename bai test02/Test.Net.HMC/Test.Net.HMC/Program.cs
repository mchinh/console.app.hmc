﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Threading;
using System.Collections;

namespace Test.Net.HMC
{
    class Program
    {
        static void Main(string[] args)
        {
            List<User> ListUser = new UsersCollections().ToList();


            #region Bai 01

            /*
             *   1. Hiển thị danh sách top 100 users 
             *   + LastName là "Nguyen Van" và FirstName kết thúc bằng "1".
             *   + Order asc theo FirstName.
             */

            var listUser01 = from usersCollection in ListUser
                where usersCollection.LName.Equals("Nguyen Van") && usersCollection.FName.EndsWith("1")
                orderby usersCollection.FName ascending
                select usersCollection;

            foreach (var item in listUser01)
            {
                Console.WriteLine(item.LName + "" + item.FName);
            }

            #endregion


            #region Bai 02

            /*
             * 2. Hiển thị top 100 users có LastName bắt đầu bằng "Nguyen".
             *       + Order desc theo FirstName. 
             *       + Hiển thị thông tin summary theo từng LastName. (vd. Nguyen Van : 10 peoples, Nguyen Dinh; 11 peoples,...).
             */
            Console.WriteLine("---------------Bai 2-----------------------");
            var listUser02 = (from users in ListUser
                where users.LName.StartsWith("Nguyen")
                select users).Take(100);
            Console.WriteLine("---------------Bai 2 List 01-----------------------");
            foreach (var item in listUser02)
            {
                Console.WriteLine(item.FName + " " + item.LName);
            }

            var ListUser22 = from r in listUser02
                orderby r.LName
                group r by r.LName
                into grp
                select new {name = grp.Key, count = grp.Count()};
            Console.WriteLine("---------------Bai 2 List 02-----------------------");
            foreach (var item in ListUser22)
            {
                Console.WriteLine(item.name + " : " + item.count + " people");
            }


//            var listUser03 = ListUser.FindAll(x => x.FName.Equals("Nguyen"))
//                .Take(100)
//                .GroupBy(X => X.LName)
//                .Select(y => new {name = y.Key, count = y.Count()});

            #endregion


            #region Bai 03

            Console.WriteLine("-------------3.1------------");
            /*
             * 3. Giả sử PageSize = 50 records, viết chương trình sao cho:
             *      + Hiển thị thông tin ban đầu gồm TotalPages, TotalRecords.
             *      + Users hiển thị order theo LastName, nếu trùng order tiếp theo FirstName, nếu trùng order desc tiếp theo Age
             *      + Nhập một pageNo bất kì, nếu PageNo > TotalPages hoặc < 1 thì showmessage "Please enter pageNo from 1 to <TotalPages>". Nếu pageNo hợp lệ => hiển thị users của page đó.
             */
            var TotalRecord = ListUser.Count;
            var TotalPages = ListUser.Count / 50;

            Console.WriteLine("TotalRecord :" + TotalRecord);
            Console.WriteLine("TotalPages :" + TotalPages);

            var listUser03 = from users in ListUser
                orderby users.LName, users.FName, users.Age descending
                select users;

            foreach (var item in listUser03)

            {
                Console.WriteLine(item.LName + " " + item.FName);
            }

            Console.WriteLine("Enter PageNo:");
            int PageNo = 0;

            try
            {
                PageNo = Convert.ToInt32(Console.ReadLine());
            }
            catch (Exception e)
            {
                Console.WriteLine("PageNo Must be NUMBER ");
            }


            if (PageNo > TotalPages)
            {
                Console.WriteLine("Please enter pageNo from 1 to " + TotalPages);
            }
            else if (PageNo >= 1 && PageNo <= TotalPages)
            {
//                List<User> arrayList = ListUser.GetRange(PageNo-1,PageNo*50);
//
//                foreach (var item in arrayList)
//                {
//                    Console.WriteLine(item.FName);
//                }
                var listUserPageInPage = ListUser.Select(s => new {s.FName, s.LName, s.Age})
                    .Skip(PageNo * 50 - 50)
                    .Take(50);

                foreach (var item in listUserPageInPage)
                {
                    Console.WriteLine(item.LName + "" + item.FName + "");
                }
            }

            #endregion


            #region Bai 04

            /*
             * 4. Hiển thị 2 loại thông tin được Group như sau.
             *  + Tổng số tuổi của người có LastName "Nguyen Van" và FirstName không chứa số "2".
             *  + Tổng số tuổi của người có LastName "Nguyen" nhưng k có tên đệm là "Van" và FirstName phải chứa số "2".
             */
            Console.WriteLine("-----------Bai 04 list 01----------");

            var ListUser05 = ListUser.Where(u => u.LName.Equals("Nguyen Van") && !u.FName.Contains("2"))
                .GroupBy(u => new
                {
                    u.Age,
                    u.FName,
                    u.LName
                })
                .Select(y => new {name = y.Key.LName, firstName = y.Key.FName, sumAge = y.Sum(e => e.Age)});

            foreach (var item in ListUser05)
            {
                Console.WriteLine("LastName:   " + item.name + item.firstName + "   Sum age:" + item.sumAge);
            }

            Console.WriteLine("-----------Bai 04 list 02----------");

            var ListUser06 = ListUser
                .Where(u => u.LName.Contains("Nguyen") && !u.LName.Contains("Van") && u.FName.Contains("2"))
                .GroupBy(u => new
                {
                    u.Age,
                    u.FName,
                    u.LName
                })
                .Select(y => new {name = y.Key.LName, firstName = y.Key.FName, sumAge = y.Sum(e => e.Age)});

            foreach (var item in ListUser06)
            {
                Console.WriteLine("LastName:   " + item.name + item.firstName + "   Sum age:" + item.sumAge);
            }
        

            #endregion

            #region Bai 05
            Console.WriteLine("-----------Bai 05 ----------");
            /*
             * 5. Hiển thị top 100 users Order theo LastName và, 
             *   + nếu LastName "Nguyen Van" thi order desc theo FirstName, 
             *   + Nếu LastName "Nguyen Dinh" thi order asc theo FirstName 
             */

            var listUser = (ListUser.Select(s => new {s.Age, s.LName, s.FName})
                    .Take(100)
                    .OrderBy(s => s.FName))
                .OrderByDescending(s => s.FName.Equals("Nguyen Van") ? s.FName : s.LName)
                .OrderBy(s => s.LName.Equals("Nguyen Dinh") ? s.FName : s.LName);
            foreach (var item in listUser)
            
                Console.WriteLine("Name"+ item.LName + item.FName +   "age" +item.Age);
            }
            #endregion
        
    }
}