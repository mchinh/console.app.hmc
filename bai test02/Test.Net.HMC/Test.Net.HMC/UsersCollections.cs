﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Test.Net.HMC
{
    public class UsersCollections: IEnumerable<User>
    {
        public static readonly string[] LastNames = new string[]
        {
            "Tran Van",
            "Nguyen Van",
            "Ngo Van",
            "Nguyen Dinh",
            "Nguyen Xuan",
            "Nguyen Thi",
            "Le Nguyen",
        };

        private List<User> _users;

        public UsersCollections()
        {
            _users = new List<User>();
            UsersInitialize();
        }

        public IEnumerator<User> GetEnumerator()
        {
            return _users.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        private void UsersInitialize()
        {
            Random rd = new Random();
            for (int i = 0; i < 100; i++)
            {
                _users.Add(new User()
                {
                    Age = rd.Next(0, 100),
                    FName = $"User{(i + 1)}",
                    LName = LastNames[rd.Next(0, 7)]
                });
            }
        }
    }

    public class User
    {
        public string FName { get; set; }
        public string LName { get; set; }
        public int Age { get; set; }
    
    }
}